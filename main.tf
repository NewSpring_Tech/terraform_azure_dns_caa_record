resource "azurerm_dns_cname_record" "cname_record" {
  name                = "${var.caa_record_name}"
  zone_name           = "${var.zone_name}"
  resource_group_name = "${var.resource_group_name}"
  ttl                 = "${var.ttl}"
  records             = "${var.records}"
  tags                = "${var.tags}"
}

